from django.shortcuts import render
from .models import Matches, Deliveries
from django.db.models import Count, Sum, Avg
from collections import OrderedDict
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

@cache_page(CACHE_TTL)
def data_project_1(request):
    number_of_matches_played_per_season = Matches.objects.values(
        "season").annotate(Count("season")).order_by('season')
    games_per_season = {}
    for value in number_of_matches_played_per_season:
        games_per_season[str(value['season'])] = value['season__count']

    return render(request, 'DataProject/data1.html', {'games_per_season': games_per_season})

@cache_page(CACHE_TTL)
def data_project_2(request):
    matches_won_by_teams_per_season = Matches.objects.values(
        "season", "winner") .annotate(Count('winner'))
    teams_winning_per_season = {}
    season_winning_list = [2008, 2009, 2010, 2011,
                           2012, 2013, 2014, 2015, 2016, 2017]
    for values in matches_won_by_teams_per_season:
        if values['winner'] not in teams_winning_per_season:
            teams_winning_per_season[str(values['winner'])] = [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            index_of_season = season_winning_list.index(values['season'])
            teams_winning_per_season[str(
                values['winner'])][index_of_season] = values['winner__count']

        else:

            index_of_season = season_winning_list.index(values['season'])
            teams_winning_per_season[str(
                values['winner'])][index_of_season] = values['winner__count']

    return render(request, 'DataProject/data2.html', {'teams_winning_per_season': teams_winning_per_season})

@cache_page(CACHE_TTL)
def data_project_3(request):
    Match_ids = Matches.objects.only("season", "id").filter(season=2016)
    extra_run_conceded_per_team = Deliveries.objects.values(
        "match_id", "bowling_team").filter(match_id__in=Match_ids).annotate(Sum("extra_runs"))
    teamname_and_extra_runs_conceded = {}
    for value in extra_run_conceded_per_team:
        teamname_and_extra_runs_conceded[str(
            value['bowling_team'])] = value['extra_runs__sum']
    return render(request, 'DataProject/data3.html', {'teamname_and_extra_runs_conceded': teamname_and_extra_runs_conceded})

@cache_page(CACHE_TTL)
def data_project_4(request):
    Match_ids = Matches.objects.only("season", "id").filter(season=2015)
    bowler_with_economy = Deliveries.objects.values("bowler").filter(
        match_id__in=Match_ids).annotate(economy=Avg("total_runs")*6).order_by('economy')[:10]

    most_economical_bowlers = {}
    for value in bowler_with_economy:
        most_economical_bowlers[str(value['bowler'])] = value['economy']
    return render(request, 'DataProject/data4.html', {'most_economical_bowlers': most_economical_bowlers})

@cache_page(CACHE_TTL)
def data_project_5(request):
    most_wicket_taker = Deliveries.objects.values("bowler").filter(
        dismissal_kind='caught').annotate(count=Count("dismissal_kind")).order_by('-count')[:10]
    top_ten_most_wicket_taker_bowler = {}
    for value in most_wicket_taker:
        top_ten_most_wicket_taker_bowler[str(value['bowler'])] = value['count']
    return render(request, 'DataProject/data5.html', {'top_ten_most_wicket_taker_bowler': top_ten_most_wicket_taker_bowler})
